HEARFEED PROJECT
================

HearFeed is a collaborative platform to improve the spoken fluency of a foreign language.
It's based on Python/Django and uses PostGreSQL database.

INSTALLATION GUIDE
__________________

Create a virtualenv using virtualenvwrapper, clone the repo, install required libraries using pip:

$ pip install -r requirements.txt


HOW TO USE IT
_____________

HearFeed is based on a simple concept that we called "cross-validated team learning": you can simultaneously improve 
your reading and listening skills, besides constantly learning new words and reading/sharing interesting pieces of text.

WORKFLOW:
* Each user should periodically record and share short audio files, specifically chosen for this purpose
* The rest of the team will provide mutual feedbacks to these files

Suitable scripts for the audio recording are concise and self-contained fragments, such as Ted.com video descriptions 
or any other brief article or abstract.



CREDITS
_______

This is a 3-days side project developed by Alex Casalboni ( @alex_casalboni ) and Antonio Angelino ( @AntoAngelino )

LICENSE
_______

Copyright 2014 Alex Casalboni and Antonio Angelino

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, 
software distributed under the License is distributed on 
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
either express or implied. See the License for the specific 
language governing permissions and limitations under the License.


Enjoy!