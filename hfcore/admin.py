from django.contrib import admin
from hfcore import models
# Register your models here.

admin.site.register(models.Content)
admin.site.register(models.Feedback)
