"""
Django settings for hearfeed project.

LOCAL ENVIRONMENT
"""

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

INTERNAL_IPS = [ '93.35.200.49' ]

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS += (
    'debug_toolbar',
)


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'hearfeed',                      # Or path to database file if using sqlite3.
            # The following settings are not used with sqlite3:
         'USER': 'hearfeed',
         'PASSWORD': 'hearfeed6969',
         'HOST': 'localhost',                      # Empty for localhost through domain sockets or           '127.0.0.1' for localhost through TCP.
            'PORT': '',                      # Set to empty string for default.
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'hfcore/static/').replace('\\', '/')

#UPLOADED MEDIA
MEDIA_ROOT = os.path.join(BASE_DIR, 'uploaded/').replace('\\', '/')
MEDIA_URL = '/uploaded/'

