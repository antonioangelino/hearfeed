$(function(){

	function play(text){
		if(speechSynthesis.speaking) {
			return speechSynthesis.cancel();
		}
		var phrases = text
			.trim()
			.split(".") //splitting by phrase allows longer texts to be reproduces
			.map(function(t){if(t)return new SpeechSynthesisUtterance(t+".");})
			.filter(function(t){return t?true:false;});
		if(!phrases.length) return;

		$.each(phrases, function(i, u){
			speechSynthesis.speak(u);	
		});		
		
	}

	function getSelectionText(textarea){
		var text = "";
		if(window.getSelection){
			text = window.getSelection().toString();
		}else if(document.selection && document.selection.type != 'Control'){
			text = document.selection.createRange().text;
		}
		return text;
	}

	if(window.SpeechSynthesisUtterance){

		$('textarea[name="transcript"],.audio-container blockquote').each(function(){
			var $this = $(this),
			    placeholder = $this.attr('placeholder'),
			    parent = $this.parent(),
			    btn = $('<button/>')
				.addClass('btn btn-default')
				.text('Play/Stop (selected) Text')
				.appendTo($('<div/>').addClass('btn-play-container').appendTo(parent));

			btn.click(function(e){
				e.preventDefault();
				var text = $this.data('selected-text') || $this.val() || $this.text();
				if(text){
					play(text);
				}else{
					console.log('nothing to play');
					$this.attr('placeholder', 'Nothing to play!');
					setTimeout(function(){
						$this.attr('placeholder', placeholder);
					}, 5000);
				}
			});
	
			$this.on('click', function(){
				$this.data('selected-text', getSelectionText());
			});

		});

	}
	

});
