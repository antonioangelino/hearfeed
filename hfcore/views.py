import logging as log
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponseNotFound
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template import RequestContext
from hfcore.models import Feedback,Content
from hfcore.forms import FeedbackForm,ContentForm

def t(name, request, params={}):
    return render_to_response(name, params, RequestContext(request))

def home(request):
    return t('home.html', request)

@login_required
def content_add(request):
    if request.method == "POST":
	print request.FILES.dict()
	content = Content(author=request.user) #,audioFile=request.FILES['file'])
	form = ContentForm(request.POST, request.FILES, instance=content)
	if form.is_valid():
            form.save()
	    messages.success(request, 'Your content has been saved!')
            return t('content_success.html', request)
	else:
    	    return t('content_add.html', request, {'form': form})
    else:
	form = ContentForm()
        return t('content_add.html', request, {'form': form})

@login_required
def content_post(request):
    if request.method == "GET":
	return redirect('hfcore.views.content_add')
    return content_add(request) #this is a joke!

@login_required
def feedbacks_list(request):
    contents = Content.objects.filter(author=request.user.id)
    return t('feedbacks_list.html', request, {'contents': contents})


@login_required
def feedback_add(request):
    cid = request.session.get('cid') #None by default
    if cid:
        log.info('Using session-stored cid: %s' % cid)
	c = Content.objects.get(pk=cid) #raises DoesNotExist
	data = None
	if request.method == 'POST':
	    feedback = Feedback(author=request.user, content=c)
	    form = FeedbackForm(request.POST, instance=feedback)
	    log.info("is form valid? %s" % form.is_valid())
	    log.info("is form bound? %s" % form.is_bound)
	    log.info(form.errors)
	else:
	    form = FeedbackForm()
    elif request.method == 'GET':
	log.info('Going to generate new random content')
        c = Content.get_random(request.user)
	if not c:
	    messages.info(request, 'No more content to review! Come back soon!')
	    return t('feedback_done.html', request)
        cid = request.session['cid'] = c.id
	form = FeedbackForm()
    else:
	return HttpResponseNotFound("No session content")

    if request.method == "POST" and form.is_valid():
        form.save()
	del request.session['cid']
	messages.success(request, 'Your feedback has been saved!')
        return t('feedback_success.html', request)
    else:
        return t('feedback_add.html', request, {'form': form, 'content': c})

@login_required
def feedback_post(request):
    if request.method == "GET":
	return redirect('hfcore.views.feedback_add')
    return feedback_add(request) #this is a joke!

