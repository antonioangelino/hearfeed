from django.db import models
from django.contrib.auth.models import User
from tinymce.models import HTMLField


class Content(models.Model):
        transcript = models.TextField("Source Text")
        createdOn = models.DateTimeField(auto_now=True)
        author = models.ForeignKey(User)
	audioFile = models.FileField("File", upload_to="audio")
	refUrl = models.URLField("Source URL")

        def __unicode__(self):
                return unicode(str(self.id))

	@classmethod
	def get_random(cls, current_user):
		return cls.objects.exclude(author=current_user.id).exclude(feedbacks__author=current_user.id).order_by('?').first()
		#return cls.objects.order_by('?').first()


class Feedback(models.Model):
        description = HTMLField("Your feedback")
        createdOn = models.DateTimeField(auto_now=True)
	rating = models.PositiveIntegerField("Rate it (0 to 9)", default=3, max_length=1)
        author = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
        content = models.ForeignKey(Content, related_name='feedbacks')
        def __unicode__(self):
                return unicode("FB" + str(self.id) + "(rating " + str(self.rating) + ")")



