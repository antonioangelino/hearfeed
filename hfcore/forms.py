from django.forms import ModelForm
from models import Feedback, Content


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = ['rating', 'description']
 	exclude = ('author','content',)

class ContentForm(ModelForm):
    class Meta:
        model = Content
        fields = ['transcript', 'refUrl', 'audioFile']
        exclude = ('author',)
