from django.conf.urls import patterns, include, url
from hfcore import views
from django.contrib import admin
from django.conf import settings
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.home),
    url(r'^content/new/$', views.content_add),
    url(r'^content/save/$', views.content_post),
    url(r'^feedbacks/$', views.feedbacks_list),
    url(r'^feedbacks/new/$', views.feedback_add),
    url(r'^feedbacks/save/$', views.feedback_post),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {
            "template_name": "login.html"}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^tinymce/', include('tinymce.urls')),
)

#if settings.DEBUG:
urlpatterns += patterns('',
        url(r'^uploaded/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
        }),
)
