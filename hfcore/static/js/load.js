$(function(){

	  var audio_context;
	  var recorder;
	  var recordingslist = $('#recordingslist');

	  function startUserMedia(stream) {
	    var input = audio_context.createMediaStreamSource(stream);
	    console.log('Media stream created.' );
	    console.log("input sample rate " +input.context.sampleRate);
	    
	    input.connect(audio_context.destination);
	    console.log('Input connected to audio context destination.');
	    
	    recorder = new Recorder(input, {
		'workersPrefix': "/static/lib/recordmp3/",
		'uploadURL': '', //TODO
		'preSubmit': function(formdata){
			//TODO add to formdata obj every form field!
		},
		'afterSubmit': function(data){
			//TODO manage server response
		}
	    });
	    console.log('Recorder initialised.');
	  }

	  /*function createDownloadLink() {
	    recorder && recorder.exportWAV(function(blob) {
	        var url = URL.createObjectURL(blob);
		var li = $('<li/>');
		var au = document.createElement('audio');
		var hf = document.createElement('a');
		au.controls = true;
		au.src = url;
		hf.href = url;
		hf.download = new Date().toISOString() + '.wav';
		hf.innerHTML = hf.download;
		li.append(au);
		li.append(hf);
		recordingslist.append(li);
	    });
	  }*/

	var startBtns = $('.start-recording').click(function(e) {
		e.preventDefault();
		recorder && recorder.record();
		$(this).attr('disabled', true);
	   	stopBtns.removeAttr('disabled');
	 	console.log('Recording...');
	  });

	var stopBtns = $('.stop-recording').click(function(e) {
		e.preventDefault();
		recorder && recorder.stop();
	        $(this).attr('disabled', true);
	        startBtns.removeAttr('disabled');
	        console.log('Stopped recording.');
	    
	        // create WAV download link using audio data blob
	        //createDownloadLink();
	    
	        recorder.clear();
	  });

	    try {
	      // webkit shim
	      window.AudioContext = window.AudioContext || window.webkitAudioContext;
	      navigator.getUserMedia = ( navigator.getUserMedia ||
		               navigator.webkitGetUserMedia ||
		               navigator.mozGetUserMedia ||
		               navigator.msGetUserMedia);
	      window.URL = window.URL || window.webkitURL;
	      
	      audio_context = new AudioContext;
	      console.log('Audio context set up.');
	      console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
	    } catch (e) {
	      alert('No web audio support in this browser!');
	    }
	    
	    navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
	      console.log('No live audio input: ' + e);
	    });

});
